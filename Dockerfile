FROM python:3.6-alpine

RUN adduser -D njeru

WORKDIR /home/njeru

COPY requirements.txt requirements.txt
RUN python3 -m venv venv
RUN pip install --upgrade pip
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn pymysql


COPY app app
COPY migrations migrations
COPY run.py config.py boot.sh ./
RUN chmod a+x boot.sh

ENV FLASK_APP run.py

RUN chown -R njeru:njeru ./
USER njeru

EXPOSE 8060:5000
ENTRYPOINT ["./boot.sh"]
