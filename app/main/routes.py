from datetime import datetime
from flask import render_template, flash, redirect, url_for, request, g, \
    jsonify, current_app
from flask_login import current_user, login_required
from flask_babel import _, get_locale
from guess_language import guess_language
from app import db
from app.main.forms import EditProfileForm, PostForm, SearchForm, MessageForm, CompanyForm
from app.models.users import User ,Notification ,Message
from app.translate import translate
from app.main import bp
from app.models import *


@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
        g.search_form = SearchForm()
    g.locale = str(get_locale())


@bp.route('/', methods=['GET'])
@bp.route('/index', methods=['GET'])
def index():
    return render_template('home/index.html', title=_('Home - lanster'))

@bp.route('/about', methods=['GET'])
def about():
    return render_template('home/about.html', title=_('About-Us - lanster'))

@bp.route('/services', methods=['GET'])
def services():
    return render_template('home/services.html', title=_('services - lanster'))

@bp.route('/contact', methods=['GET'])
def contact():
    return render_template('home/contact.html', title=_('Contact-US - lanster'))


@bp.route('/company', methods=['GET', 'POST'])
@login_required
def create_company():
    form = CompanyForm()
    if form.validate_on_submit():
        data = request.form.to_dict(flat=False)
        company = Company()
        company.user_id = current_user.id
        current_user.isCompanySet = 'yes'
        company.from_dict(data)
        db.session.add(company)
        db.session.commit()
        flash(_('Company Created!'))
        return redirect(url_for('main.index'))
    return render_template('create_company.html', title=_('Home'), form=form)


@bp.route('/explore')
@login_required
def explore():
    page = request.args.get('page', 1, type=int)
    posts = Vacancy.query.order_by(Vacancy.timestamp.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.explore', page=posts.next_num) \
        if posts.has_next else None
    prev_url = url_for('main.explore', page=posts.prev_num) \
        if posts.has_prev else None
    vacancies = Vacancy.query.filter_by(company_id = current_user.company[0].id)
    return render_template('index.html', title=_('Explore'),
                           posts=posts.items, next_url=next_url,
                           prev_url=prev_url,vacancies=vacancies)


@bp.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    page = request.args.get('page', 1, type=int)
    posts = user.posts.order_by(Vacancy.timestamp.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.user', username=user.username,
                       page=posts.next_num) if posts.has_next else None
    prev_url = url_for('main.user', username=user.username,
                       page=posts.prev_num) if posts.has_prev else None

    vacancies  = Vacancy.query.filter_by(company_id = current_user.company[0].id)
    return render_template('user.html', user=user, posts=posts.items,
                           next_url=next_url, prev_url=prev_url, vacancies=vacancies )


@bp.route('/user/<username>/popup')
@login_required
def user_popup(username):
    user = User.query.filter_by(username=username).first_or_404()
    return render_template('user_popup.html', user=user)


@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash(_('Your changes have been saved.'))
        return redirect(url_for('main.edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title=_('Edit Profile'),
                           form=form)


@bp.route('/follow/<username>')
@login_required
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash(_('User %(username)s not found.', username=username))
        return redirect(url_for('main.index'))
    if user == current_user:
        flash(_('You cannot follow yourself!'))
        return redirect(url_for('main.user', username=username))
    current_user.follow(user)
    db.session.commit()
    flash(_('You are following %(username)s!', username=username))
    return redirect(url_for('main.user', username=username))


@bp.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash(_('User %(username)s not found.', username=username))
        return redirect(url_for('main.index'))
    if user == current_user:
        flash(_('You cannot unfollow yourself!'))
        return redirect(url_for('main.user', username=username))
    current_user.unfollow(user)
    db.session.commit()
    flash(_('You are not following %(username)s.', username=username))
    return redirect(url_for('main.user', username=username))


@bp.route('/translate', methods=['POST'])
@login_required
def translate_text():
    return jsonify({'text': translate(request.form['text'],
                                      request.form['source_language'],
                                      request.form['dest_language'])})

@bp.route('/search')
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('main.explore'))
    page = request.args.get('page', 1, type=int)
    posts, total = Vacancy.search(g.search_form.q.data, page,
                               current_app.config['POSTS_PER_PAGE'])
    next_url = url_for('main.search', q=g.search_form.q.data, page=page + 1) \
        if total > page * current_app.config['POSTS_PER_PAGE'] else None
    prev_url = url_for('main.search', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None
    return render_template('search.html', title=_('Search'), posts=posts,
                           next_url=next_url, prev_url=prev_url)


@bp.route('/send_message/<recipient>', methods=['GET', 'POST'])
@login_required
def send_message(recipient):
    user = User.query.filter_by(username=recipient).first_or_404()
    form = MessageForm()
    if form.validate_on_submit():
        msg = Message(author=current_user, recipient=user,
                      body=form.message.data)
        db.session.add(msg)
        user.add_notification('unread_message_count', user.new_messages())
        db.session.commit()
        flash(_('Your message has been sent.'))
        return redirect(url_for('main.user', username=recipient))
    return render_template('send_message.html', title=_('Send Message'),
                           form=form, recipient=recipient)


@bp.route('/messages')
@login_required
def messages():
    current_user.last_message_read_time = datetime.utcnow()
    current_user.add_notification('unread_message_count', 0)
    db.session.commit()
    page = request.args.get('page', 1, type=int)
    messages = current_user.messages_received.order_by(
        Message.timestamp.desc()).paginate(
            page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.messages', page=messages.next_num) \
        if messages.has_next else None
    prev_url = url_for('main.messages', page=messages.prev_num) \
        if messages.has_prev else None
    return render_template('messages.html', messages=messages.items,
                           next_url=next_url, prev_url=prev_url)


@bp.route('/edit_posts', methods=['GET','POST'])
@login_required
def edit_posts():
    company = current_user.company[0]
    form = CompanyForm(obj=company)
    if form.validate_on_submit():
        data = request.form.to_dict(flat=False)
        company.user_id = current_user.id
        company = company.from_dict(data)
        db.session.commit()
        flash(_('Your changes have been saved.'))
        return redirect(url_for('main.index'))
    return render_template('edit_company.html', title=_('Edit Company'),
                           form=form)
@bp.route('/notifications')
@login_required
def notifications():
    since = request.args.get('since', 0.0, type=float)
    notifications = current_user.notifications.filter(
        Notification.timestamp > since).order_by(Notification.timestamp.asc())
    return jsonify([{
        'name': n.name,
        'data': n.get_data(),
        'timestamp': n.timestamp
    } for n in notifications])

@bp.route('/more')
@login_required
def more():

    return render_template('more.html')

@bp.route('/view_vacancies')
@login_required
def view_vacancies():
    vacancies = current_user.company[0].vacancy
    return render_template('view_vacancies.html', vacancies=vacancies)
@bp.route('/view_applicants')
@login_required
def view_applicants():
    applicants  = Candidate.query.all()
    '''Candidate.query.filter_by(company_id=company.id).all()'''
    applicants = applicants
    return render_template('view_applicants.html',applicants = applicants)

@bp.route('/view/<applicant_id>')
@login_required
def view_applicant(applicant_id):
    vacancy = Candidate.query.filter_by(id = applicant_id).first_or_404()
    if vacancy:
        db.session.delete(vacancy)
        db.session.commit()
    return redirect(url_for('main.view_applicants'))
@bp.route('/accept/<applicant_id>')
@login_required
def decline_applicant(applicant_id):
    vacancy = Candidate.query.filter_by(id = applicant_id).first_or_404()
    if vacancy:
        vacancy.status= "declined"
        db.session.commit()
    return redirect(url_for('main.view_applicants'))

@bp.route('/accept/<applicant_id>')
@login_required
def accept_applicant(applicant_id):
    vacancy = Candidate.query.filter_by(id = applicant_id).first_or_404()
    if vacancy:
        vacancy.status= "accepted"
        db.session.commit()
    return redirect(url_for('main.view_applicants'))
@bp.route('/deletecandidate/<applicant_id>')
@login_required
def delete_applicant(applicant_id):
    vacancy = Candidate.query.filter_by(id = applicant_id).first_or_404()
    if vacancy:
        db.session.delete(vacancy)
        db.session.commit()
    return redirect(url_for('main.view_applicants'))


@bp.route('/deletevacancy/<vacancy_id>')
@login_required
def delete(vacancy_id):
    vacancy = Vacancy.query.filter_by(id = vacancy_id).first_or_404()
    if vacancy:
        db.session.delete(vacancy)
        db.session.commit()
    return redirect(url_for('main.view_vacancies'))

@bp.route('/edit/<vacancy_id>', methods=['GET','POST'])
@login_required
def edit(vacancy_id):
    vacancy = Vacancy.query.filter_by(id = vacancy_id).first_or_404()
    form = PostForm(obj=vacancy)
    if vacancy:
        if form.validate_on_submit():
            data = request.form.to_dict(flat=False)
            vacancy.user_id = current_user.id
            vacancy = vacancy.from_dict(data)
            db.session.commit()
            flash(_('Your changes have been saved.'))
            return redirect(url_for('main.view_vacancies'))
    return render_template('edit_vacancy.html', form=form)
