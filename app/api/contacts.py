from flask import jsonify, request, url_for
from app import db
from app.models import *
from app.api import bp
from app.api.auth import token_auth
from app.api.errors import bad_request
from flask_login import current_user
# todo
# get one contact
@bp.route('/contacts/<id>', methods=['GET'])
@token_auth.login_required
def get_contactByToken(id):
    user = User.query.filter_by(id = id).first()
    contacts = Contacts.query.filter_by(user_id = user.id).all()
    return jsonify(Contacts.to_dict_col(contacts))

@bp.route('/contact/<int:id>', methods=['GET'])
@token_auth.login_required
def get_contactById(id):

    return jsonify(Contacts.query.get_or_404(id).to_dict())

# get all contacts
# admin users
@bp.route('/contacts', methods=['GET'])
@token_auth.login_required
def get_contacts():
    contacts = Contacts.query.all()
    
    return jsonify(Contacts.to_dict_col(contacts=contacts))

# batch upload of contacts
@bp.route('/contacts', methods=['POST'])
@token_auth.login_required
def create_contacts():
    data = request.get_json() or {}
    
    for contac in data:
        contact = Contacts()
        contact.from_dict(contac)
        db.session.add(contact)
        db.session.commit()

    response = jsonify(data)
    response.status_code = 200
    response.headers['Location'] = url_for('api.get_contacts')
    return response

# upload one contact
@bp.route('/contact', methods=['POST'])
# @token_auth.login_required
def create_contact():
    data = request.get_json() or {}
    id = data['user_id']
    user = User.query.filter_by(id = id).first() 
    data['user_id'] = user.id    
    contact = Contacts()
    contact.from_dict(data)
    db.session.add(contact)
    db.session.commit()
    response = jsonify(contact.to_dict())
    response.status_code = 200
    response.headers['Location'] = url_for('api.create_contact', id=contact.id)
    return response

# update one contact for specific user
@bp.route('/contact/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_contact(id):
    # get contact user
    user = User.query.filter_by(id = id).first()
    contact = Contacts.query.get_or_404(user.id)
    data = request.get_json() or {}
    contact.from_dict(data)
    db.session.commit()
    return jsonify(contact.to_dict())

# delete one contact for specific user
@bp.route('/contact/<int:id>', methods=['DELETE'])
@token_auth.login_required
def delete_contact(id):
    # get contact user
    user = User.query.filter_by(id = id).first()
    contact = Contacts.query.get_or_404(user.id)
    if not contact:
        return jsonify({'status':'deletion not successful'})
    else:
        db.session.delete(contact)
        db.session.commit()
        return jsonify({'status':'deletion successful'})
        
