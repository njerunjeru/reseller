from flask import jsonify, request, url_for
from app import db
from app.models import *
from app.api import bp
from app.api.auth import token_auth
from app.api.errors import bad_request

@bp.route('/transactions/<int:id>', methods=['GET'])
@token_auth.login_required
def get_transaction(id):

    transactions = Transactions.query.filter_by(user_id = id).all()

    transactions = Transactions.to_dict_col(transactions)
    
    return jsonify(transactions)


@bp.route('/transactions', methods=['GET'])
@token_auth.login_required
def get_transactions():
    transactions = Transactions.query.all()
    
    return jsonify(Transactions.to_dict_col(transactions=transactions))

# dont use this endpoint
@bp.route('/transactions', methods=['POST'])
@token_auth.login_required
def create_transactions():
    data = request.get_json() or {}
    
    transaction = Transactions()
    transaction.from_dict(data)
    db.session.add(transaction)
    db.session.commit()
    response = jsonify(transaction.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_transaction', id=transaction.id)
    return response

# dont use
@bp.route('/transactions/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_transaction(id):
    transaction = Transactions.query.get_or_404(id)
    data = request.get_json() or {}
    
    transaction.from_dict(data)
    db.session.commit()
    return jsonify(transaction.to_dict())

# dont use this
@bp.route('/transactions/<int:id>', methods=['DELETE'])
@token_auth.login_required
def delete_transaction(id):
    transaction = Transactions.query.get_or_404(id)
    if transaction:
        db.session.delete(transaction)
        db.session.commit()
        return jsonify({'status':'transaction deleted successifully'})
    else:
        return jsonify({'status':'transaction not deleted successifully'})
