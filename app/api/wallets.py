from flask import jsonify, request, url_for
from app import db
from app.models import *
from app.api import bp
from app.api.auth import token_auth
from app.api.errors import bad_request
from flask_login import current_user



@bp.route('/walletsbyid/<int:id>', methods=['GET'])
@token_auth.login_required
def get_wallet(id):

    return jsonify(Wallets.query.get_or_404(id =id).to_dict())


@bp.route('/wallets/<int:id>', methods=['GET','POST'])
@token_auth.login_required
def get_wallets(id):
    user = User.query.filter_by(id = id).first()
    wallets = Wallets.query.filter(Wallets.user_id == user.id).first().to_dict()
    return jsonify(wallets)


@bp.route('/wallets', methods=['POST'])
@token_auth.login_required
def create_wallet():
    data = request.get_json() or {}

    wallet = Wallets()
    wallet.from_dict(data)
    db.session.add(wallet)
    db.session.commit()
    response = jsonify(wallet.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_wallet', id=wallet.id)
    return response


@bp.route('/wallets/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_wallet(id):
    wallet = Wallets.query.get_or_404(id)
    data = request.get_json() or {}
    wallet.from_dict(data)
    db.session.commit()
    return jsonify(wallet.to_dict())

@bp.route('/wallets/<int:id>', methods=['DELETE'])
@token_auth.login_required
def delete_wallet(id):
    wallet = Wallets.query.get_or_404(id)
    if not wallet:
        return jsonify({'status':'deletion not successful'})
    else:
        db.session.delete(wallet)
        db.session.commit()
        return jsonify({'status':'deletion successful'})
        
# mpesa responses
'''

    {
        'MerchantRequestID': '7929-2055949-1', 
        'CheckoutRequestID': 'ws_CO_DMZ_529352970_21062019090959534',
        'ResponseCode': '0', 
        'ResponseDescription': 'Success. Request accepted for processing',
        'CustomerMessage': 'Success. Request accepted for processing'
    }
///
  {
        "Body":{
        "stkCallback":{
        "MerchantRequestID":"19465-780693-1",
        "CheckoutRequestID":"ws_CO_27072017154747416",
        "ResultCode":0,
        "ResultDesc":"The service request is processed successfully.",
        "CallbackMetadata":{
        "Item":[
        {
        "Name":"Amount",
        "Value":1
        },
        {
        "Name":"MpesaReceiptNumber",
        "Value":"LGR7OWQX0R"
        },
        {
        "Name":"Balance"
        },
        {
        "Name":"TransactionDate",
        "Value":20170727154800
        },
        {
        "Name":"PhoneNumber",
        "Value":254721566839
        }
        ]
        }
        }
    }
}
'''

# a cancled transaction
'''
{
    'Body': 
        {'stkCallback': 
            {   'MerchantRequestID': '9839-2092664-1',
                'CheckoutRequestID': 'ws_CO_DMZ_529360994_21062019093921870',
                'ResultCode': 1032, 
                'ResultDesc': '[STK_CB - ]Request cancelled by user'
            }
        }
}
'''

# a complete transactio

'''
    {
        'Body': 
                {'stkCallback': 
                    {   'MerchantRequestID': '23834-2287345-1', 
                        'CheckoutRequestID': 'ws_CO_DMZ_382150630_21062019094148589',
                        'ResultCode': 0, 'ResultDesc': 'The service request is processed successfully.',
                        'CallbackMetadata': 
                            {'Item': [
                                        {'Name': 'Amount', 'Value': 1.0}, 
                                        {'Name': 'MpesaReceiptNumber','Value': 'NFL9QHA4ZR'},
                                        {'Name': 'Balance'}, 
                                        {'Name': 'TransactionDate', 'Value': 20190621094200}, 
                                        {'Name': 'PhoneNumber', 'Value': 254702261679}
                                     ]
                            }
                    }
                }
    }

'''