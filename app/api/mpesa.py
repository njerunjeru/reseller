from flask import jsonify, request, url_for
from app import db
from app.models import *
from app.api import bp
from app.api.auth import token_auth
from app.api.errors import bad_request
from mpesa.api.auth import MpesaBase
from mpesa.api.mpesa_express import MpesaExpress
import base64
from flask_login import current_user
import os

'''
    @mpesa endpoints
    url https://localhost:8000/api/mpesa/stkpush
    method : get/post
    headers :Authorization Bearer token 
    ContentType :json
    body    : {
        "amount":"0.0",
        "phone":"07..",
        "token":"xxx"
    }
'''

def getmpesaauthobject():
# this keys should be in the confiration files
    consumer_key = os.environ.get('consumer_key')
    consumer_secret = os.environ.get('consumer_secret')
    mpesa_auth_object = MpesaBase("sandbox",
                                      app_key=consumer_key,
                                      app_secret=consumer_secret,
                                      sandbox_url="https://sandbox.safaricom.co.ke",
                                      live_url="https://safaricom.co.ke"
                                      )
    return mpesa_auth_object

# from libray
def gettoken():
    token = getmpesaauthobject().authenticate()
    
    return token
# my customed

import requests
from requests.auth import HTTPBasicAuth
import json

def getToken(consumer_key = 'jJMVK098pTNas1GdmiEUGwVARaI5zOs3', consumer_secret = 'LMmzbd6qzELQfR5f'):
    api_URL = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials"
    r = requests.get(api_URL, auth=HTTPBasicAuth(consumer_key, consumer_secret))
    respose = r.text
    respose = json.loads(respose)
    print (respose['access_token'])
@bp.route('/mpesa/stkpush', methods=['GET','POST'])
@token_auth.login_required
def test_stk_push():
    # required imports
    import requests
    import datetime
    from base64 import b64encode
    from pprint import pprint
    # get mpesa token
    consumer_key = "jJMVK098pTNas1GdmiEUGwVARaI5zOs3"
    consumer_secret = "LMmzbd6qzELQfR5f"

    access_token = gettoken()
    # access_token = getToken(consumer_key, consumer_secret)
    api_url = "https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest"
    headers = { "Authorization": "Bearer %s" % access_token }

    business_short_code = "174379"
    pass_key = "bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919"
 
    timestamp = getTimestamp()

    password = "{}{}{}".format(business_short_code,pass_key,str(timestamp))

    data_bytes = password.encode("utf-8")
    #password encoding base64 
    password = b64encode(data_bytes)
    # get use data as json 
    data = request.get_json() or {}
    token = data['token']
    requeststompesa = {
    "BusinessShortCode":business_short_code ,
    "Password": password.decode("utf-8"),
    "Timestamp": str(timestamp),
    "TransactionType": "CustomerPayBillOnline",
    "Amount": data["amount"],
    "PartyA": "254728336648",
    "PartyB": "174379",
    "PhoneNumber": data["phone"],
    "CallBackURL": "http://197.248.0.222:5000/api/mpesa/callback",
    "AccountReference": "2 ",
    "TransactionDesc": " Buy airtime"
    }

    # send request to mpesa
    response = requests.post(api_url, json = requeststompesa, headers=headers)
    response = response.json()
    # extracting mpesa response data
    merchantrequestid = response['MerchantRequestID']
    checkoutrequestid = response['CheckoutRequestID']
    requestcode = response['ResponseCode']
    description = response['ResponseDescription']
    # save mpesa transaction 
    mpesa = Mpesa()
    mpesa.merchantrequestid = merchantrequestid
    mpesa.checkoutrequestid = checkoutrequestid
    mpesa.requestcode = requestcode
    mpesa.status = 'initiated'
    mpesa.phonenumber = data["phone"]
    # generate a unique transaction code lanstar's code for the transaction
    # this if for our internal use
    tcode = mpesa.generateTransactioncode(10).upper()
    mpesa.transactioncode = tcode
   
    # getting user details for transacting user
    user = User.check_token(token)
    if not user:
        # either an expired token or an invalid token
        pass
    else:
        wallet = Wallets.query.filter_by(user_id = user.id).first()
        if wallet :
            # this is the usual case
            mpesa.wallet_id = wallet.id
            transaction = Transactions()
            transaction.wallet_id = wallet.id
            transaction.user_id = user.id
            transaction.amount = data["amount"]
            transaction.title = 'Top up from mpesa'
            transaction.description = 'Top up from mpesa  {}'.format(description)
            transaction.action ='in'
            transaction.transactionid = tcode
            transaction.status = 'pending'
            db.session.add(transaction)
            db.session.add(mpesa)
            db.session.commit()
        else:
            # creating a new wallet this should never happen since every user is created with a wallet
            wallet = Wallets()
            transaction = Transactions()
            wallet.body = "Main Wallet"
            wallet.description = "Top up wallet to buy messages and airtime"
            wallet.title = "Oasis"
            wallet.amount = data["amount"]
            wallet.wallet = user
            db.session.add(wallet)
            db.session.commit()
            transaction.wallet_id = wallet.id
            transaction.user_id = user.id
            transaction.amount = data["amount"]
            transaction.action ='in'
            transaction.title = 'Top up from mpesa'
            transaction.description = 'Top up from mpesa amount : {}'.format(data['amount'])
            transaction.transactionid = tcode
            transaction.status = 'pending'
            mpesa.mpesa = wallet
            db.session.add(mpesa)
            db.session.add(transaction)
            db.session.commit()
    # stkpush is complete
    return jsonify(response)

# get timestamp
def getTimestamp():
    import datetime
    st = str(datetime.datetime.now()).split(".")[0].replace("-", "").replace(" ", "").replace(":", "")
    return st

# call back from mpesa
@bp.route('/mpesa/callback', methods=['GET','POST'])
def callback():
    data = request.get_json() or {}
    # get stkpush data
    # merchantrequestid, checkoutrequestid, requestcode, phonenumber
    stkCallback = data['Body']['stkCallback']
    merchantrequestid = stkCallback['MerchantRequestID']
    checkoutrequestid = stkCallback['CheckoutRequestID']
    requestcode = stkCallback['ResultCode']
    description = stkCallback['ResultDesc']
    mpesa = Mpesa()
    phonenumber = mpesa.getPhone(merchantrequestid = merchantrequestid,checkoutrequestid =checkoutrequestid)
    mpesa = Mpesa.query.filter(Mpesa.merchantrequestid == merchantrequestid,Mpesa.checkoutrequestid==checkoutrequestid,Mpesa.phonenumber ==phonenumber).first()
    wallet = Wallets.query.filter_by(id =mpesa.wallet_id).first()
    if int(requestcode) == 0 :
        mpesa.status = 'complete'
        # get wallet for this mpesa transaction 
        # get amount from mpesa
        amount = stkCallback['CallbackMetadata']['Item'][0]['Value']
        wallet.amount = float(wallet.amount) + float(amount)
        transaction = Transactions.query.filter(Transactions.wallet_id == wallet.id, Transactions.transactionid==mpesa.transactioncode).first()
        transaction.status = 'complete'
        # update wallet here 
        db.session.commit()
        print('completing transaction') 
    else:
        # complete our transaction ie lanstar by updating our transaction table as incomplete
        mpesa.requestcode = requestcode
        mpesa.status = 'incomplete'
        transaction = Transactions.query.filter(Transactions.wallet_id == wallet.id, Transactions.transactionid==mpesa.transactioncode).first()
        transaction.status = 'declined'
        transaction.description = description
        db.session.commit()
        # send phone number message to inform incomplete transaction

    # send phone number message on status of the transaction

    return jsonify(data)