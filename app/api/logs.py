from flask import jsonify, request, url_for
from app import db
from app.models import *
from app.api import bp
from app.api.auth import token_auth
from app.api.errors import bad_request
from flask_login import current_user

@bp.route('/logs/<int:id>', methods=['GET'])
@token_auth.login_required
def get_log(id):
    return jsonify(log.query.get_or_404(id).to_dict())


@bp.route('/logs', methods=['GET'])
@token_auth.login_required
def get_logs():
    logs = log.query.all()
    
    return jsonify(log.to_dict_col(logs=logs))


@bp.route('/logs', methods=['POST'])
@token_auth.login_required
def create_logs():
    data = request.get_json() or {}

    log = log()
    log.body = data['body']
    user = User.check_token(data['token'])
    log.candidate_id = user.id
    db.session.add(log)
    db.session.commit()
    response = jsonify(log.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_log', id=log.id)
    return response


@bp.route('/logs/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_log(id):
    log = log.query.get_or_404(id)
    data = request.get_json() or {}
    
    log.from_dict(data)
    db.session.commit()
    return jsonify(log.to_dict())

@bp.route('/logs/<int:id>', methods=['DELETE'])
@token_auth.login_required
def delete_log(id):
    log = log.query.get_or_404(id)
    if log:
        db.session.delete(log)
        db.session.commit()
        return jsonify({'status':'log deleted successifully'})
    else:
        return jsonify({'status':'log not deleted successifully'})
