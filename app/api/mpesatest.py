import requests
from requests.auth import HTTPBasicAuth
import json
import os

def getToken():
    token = getmpesaauthobject().authenticate()
    return token
    
def stkpush(amount ='', phonenumber = ''):
    import requests
    import datetime
    from base64 import b64encode
    from pprint import pprint
    access_token = str(getToken())
    # access_token = getToken(consumer_key, consumer_secret)
    api_url = "https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest"
    headers = { "Authorization": "Bearer %s" % access_token }

    business_short_code = "174379"
    pass_key = "bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919"
 
    timestamp = getTimestamp()

    password = "{}{}{}".format(business_short_code,pass_key,str(timestamp))

    data_bytes = password.encode("utf-8")
    #password encoding base64 
    password = b64encode(data_bytes)
    # get use data as json 
   
    requeststompesa = {
    "BusinessShortCode":business_short_code ,
    "Password": password.decode("utf-8"),
    "Timestamp": str(timestamp),
    "TransactionType": "CustomerPayBillOnline",
    "Amount": amount,
    "PartyA": "254728336648",
    "PartyB": "174379",
    "PhoneNumber": phonenumber,
    "CallBackURL": "http://d26884b0.ngrok.io/api/mpesa/callback",
    "AccountReference": "2 ",
    "TransactionDesc": " Buy airtime"
    }

    # send request to mpesa
    response = requests.post(api_url, json = requeststompesa, headers=headers)
    response = response.json()
    print(response)
    return response
# get timestamp
def getTimestamp():
    import datetime
    st = str(datetime.datetime.now()).split(".")[0].replace("-", "").replace(" ", "").replace(":", "")
    return st

def getmpesaauthobject():
# this keys should be in the confiration files
    consumer_key = "jJMVK098pTNas1GdmiEUGwVARaI5zOs3"
    consumer_secret = "LMmzbd6qzELQfR5f"
    mpesa_auth_object = MpesaBase("sandbox",
                                      app_key=consumer_key,
                                      app_secret=consumer_secret,
                                      sandbox_url="https://sandbox.safaricom.co.ke",
                                      live_url="https://safaricom.co.ke"
                                      )
    return mpesa_auth_object


import requests
from requests.auth import HTTPBasicAuth


class MpesaBase:
    def __init__(self, env="sandbox", app_key=None, app_secret=None, sandbox_url="https://sandbox.safaricom.co.ke",
                 live_url="https://safaricom.co.ke"):
        self.env = env
        self.app_key = app_key
        self.app_secret = app_secret
        self.sandbox_url = sandbox_url
        self.live_url = live_url
        self.token = None

    def authenticate(self):
        """To make Mpesa API calls, you will need to authenticate your app. This method is used to fetch the access token
        required by Mpesa. Mpesa supports client_credentials grant type. To authorize your API calls to Mpesa,
        you will need a Basic Auth over HTTPS authorization token. The Basic Auth string is a base64 encoded string
        of your app's client key and client secret.

            **Args:**
                - env (str): Current app environment. Options: sandbox, live.
                - app_key (str): The app key obtained from the developer portal.
                - app_secret (str): The app key obtained from the developer portal.
                - sandbox_url (str): Base Safaricom sandbox url.
                - live_url (str): Base Safaricom live url.

            **Returns:**
                - access_token (str): This token is to be used with the Bearer header for further API calls to Mpesa.

            """
        if self.env == "production":
            base_safaricom_url = self.live_url
        else:
            base_safaricom_url = self.sandbox_url
        authenticate_uri = "/oauth/v1/generate?grant_type=client_credentials"
        authenticate_url = "{0}{1}".format(base_safaricom_url, authenticate_uri)
        r = requests.get(authenticate_url,
                         auth=HTTPBasicAuth(self.app_key, self.app_secret))
        self.token = r.json()['access_token']
        return r.json()['access_token']


if __name__ == '__main__':
    stkpush(amount ='1', phonenumber = '254702261679')

