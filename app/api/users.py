from flask import jsonify, request, url_for
from app import db
from app.models import *
from app.models.users import *
from app.api import bp
from app.api.auth import token_auth
from app.api.errors import bad_request
from flask_login import current_user
from flask import render_template, current_app
from flask_babel import _
@bp.route('/users/<int:id>', methods=['GET'])
@token_auth.login_required
def get_user(id):
    return jsonify(User.query.get_or_404(id).to_dict())


@bp.route('/users', methods=['GET'])
@token_auth.login_required
def get_users():
    users = User.query.all()
    
    return jsonify(User.to_dict_col(users=users))

@bp.route('/test', methods=['POST'])
def api():
    data = request.get_json() or {}
    
    return jsonify(data)

'''
url : basic_url/api/users
method : post
body : {
	"username":"njeru1",
	"email":"njeru2@gmail.com",
	"password":"12345",
    "first_name":"",
    "last_name",""
}
'''
@bp.route('/users', methods=['POST'])
def create_user():
    data = request.get_json() or {}
    if 'username' not in data or 'email' not in data or 'password' not in data:
        return bad_request('must include username, email and password fields')
    if User.query.filter_by(username=data['username']).first():
        return bad_request('please use a different username')
    if User.query.filter_by(email=data['email']).first():
        return bad_request('please use a different email address')
    user = User()
    user.from_dict(data, new_user=True)
    db.session.add(user)
    db.session.commit()
    response = jsonify(user.to_dict())
    wallet = Wallets()
    wallet.body = "Default"
    wallet.description = "Test"
    wallet.title = "Test"
    wallet.amount = 0.0
    wallet.wallet = user
    db.session.add(wallet)
    db.session.commit()
    response.status_code = 200
    response.headers['Location'] = url_for('api.get_user', id=user.id)
    return response

'''
url : base_url/api/users/{value}
method  : put
body
{
    //maintain
}
'''
@bp.route('/users/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_user(id):
    user = User.query.get_or_404(id)
    data = request.get_json() or {}
    if 'username' in data and data['username'] != user.username and \
            User.query.filter_by(username=data['username']).first():
        return bad_request('please use a different username')
    if 'email' in data and data['email'] != user.email and \
            User.query.filter_by(email=data['email']).first():
        return bad_request('please use a different email address')
    user.from_dict(data, new_user=False)
    db.session.commit()
    return jsonify(user.to_dict())

@bp.route('/send_message', methods=['GET', 'POST'])
@token_auth.login_required
def send_message():
    data = request.get_json('params') or {}
    current_sender = User.query.filter_by(username=data['sender']).first()
    if current_sender:
        user = User.query.filter_by(username=data['recipient']).first()

        if user:
            msg = Message(author=current_sender, recipient=user,
                            body=data['message'])
            db.session.add(msg)
            user.add_notification('unread_message_count', user.new_messages())
            db.session.commit()
            return jsonify({'succes':'message sent'})
        else:
            return jsonify({'fail':'use a valid recipient id'})
    else:
        return jsonify({'fail':'use a valid sender id'})



@bp.route('/messages', methods=['GET'])
@token_auth.login_required
def messages():
    data = request.get_json() or {}
    current_sender = User.query.filter_by(username=data['sender']).first()
    current_sender.last_message_read_time = datetime.utcnow()
    current_sender.add_notification('unread_message_count', 0)
    db.session.commit()
    page = request.args.get('page', 1, type=int)
    messages = current_sender.messages_received.order_by(
        Message.timestamp.desc()).paginate(
            page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.messages', page=messages.next_num) \
        if messages.has_next else None
    prev_url = url_for('main.messages', page=messages.prev_num) \
        if messages.has_prev else None
    return jsonfiy({'messages':messages.items})

    
from app.email import send_email
@bp.route('/sendmail', methods=['GET','POST'])
@token_auth.login_required
def sendmail():
    data = request.get_json() or {}
    send_email(_('[Student Alert] Email'),
               sender=current_app.config['ADMINS'][0],
               recipients=[data['recipient']],
               text_body=data['message'],
               html_body=data['message'])
    return jsonify({'status':'200','message':'sucesss'})