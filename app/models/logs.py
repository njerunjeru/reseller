import base64
from datetime import datetime, timedelta
import os
from time import time
from flask import current_app, url_for
from app import db


class Logs(db.Model):
    __tablename__='logs'
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(64), default='')
    createdat = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedat = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    @staticmethod
    def to_dict_col(logs):
        output = []
        for log in logs:
            data = {
                'id': log.id,
                'body': log.body,
            }
            
            output.append(data)
        return output
    def to_dict(self):

        data = {
                'id': self.id,
                'body': self.body,
            }
        
        return data

    def from_dict(self, data):
        for field in ['body']:
            if field in data:
                setattr(self, field, data[field])
