
from app import db
class Group(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(12), default='')
    phone = db.Column(db.String(10), nullable=False)
    amount = db.Column(db.Float, default=0.0, nullable=False)
    password = db.Column(db.String(45) ,default='')
    # user_id = db.Column(db.Integer, db.ForeignKey('users.id')) 
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))


    def getGroupAmount(self):
        return self.amount