# contact model this class creates a representation of a contact in real world
import base64
from datetime import datetime, timedelta
from hashlib import md5
import json
import os
from time import time
from app import db

# contact table which relates to user one user has many contacts i.e his/her clients
class Contacts(db.Model):
    __tablename__='contacts'
    id = db.Column(db.Integer, primary_key=True)
    phone = db.Column(db.String(64), default='') 
    '''phone number of the client'''
    lastname = db.Column(db.String(64), default='') 
    '''lastname of the client'''
    firstname = db.Column(db.String(64), default='') 
    '''firstname of the client'''
    user_id = db.Column(db.Integer, db.ForeignKey('users.id')) 
    '''creates a relationship with users table'''
    # records create time and update time
    createdat = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedat = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

# converts contact objects to a list which  is serializable
    @staticmethod
    def to_dict_col(contacts):
        output = []
        for contact in contacts:
            data = {
                'id': contact.id,
                'phone': contact.phone,
                'lastname': contact.lastname,
                'user_id':contact.user_id,
                'firstname': contact.firstname,
            }    
            output.append(data)
        return output
# converts a contact object to a dict
    def to_dict(self):
        data = {
                'id': self.id,
                'phone': self.phone,
                'lastname': self.lastname,
                'user_id':self.user_id,
                'firstname': self.firstname,     
            }
       
        return data
# sets json data to contact object
    def from_dict(self, data):
        for field in ['phone', 'lastname', 'firstname','user_id']:
            if field in data:
                setattr(self, field, data[field])
