import base64
from datetime import datetime, timedelta
from time import time
from app.models.users import User
from app import db

class Wallets(db.Model):
    __tablename__='wallets'
    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Float, default=0.00)
    title = db.Column(db.String(1000), default='')
    description = db.Column(db.String(1000), default='')
    #relationship with user
    mpesa = db.relationship('Mpesa', cascade="all,delete", backref='mpesa', lazy='dynamic')
    transation = db.relationship('Transactions', cascade="all,delete", backref='transation', lazy='dynamic')
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    @staticmethod
    def to_dict_col(wallets):
        output = []
        for wallet in wallets:
            data = {
                'id': wallet.id,
                'amount': wallet.amount,
                'user_id': wallet.user_id,
                'title': wallet.title,
                'description': wallet.description
            }
            
            output.append(data)
        return output
    def to_dict(self):
        data = {
                'id': self.id,
                'body': self.description,
                'user_id': self.user_id,
                'title': self.title,
                'amount': self.amount,
                'description': self.description
            }
       
        return data

    def from_dict(self, data):
        for field in ['description', 'user_id','amount','title']:
            if field in data:
                setattr(self, field, data[field])

    