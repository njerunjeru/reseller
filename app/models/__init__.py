"""
These imports enable us to make all defined models members of the models
module (as opposed to just their python files)
"""

from .users import User ,Role
from .transactions import *
from .logs import *
from .wallets import *
from .contacts import *
from .mpesa import *
from .groups import Group