import base64
from datetime import datetime, timedelta
from hashlib import md5
import json
import os
from time import time
from app import db

class Transactions(db.Model):
    __tablename__='transactions'
    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Float, default=0.0)
    action = db.Column(db.String(64), default='')
    status = db.Column(db.String(64), default='')
    title = db.Column(db.String(64), default='')
    description = db.Column(db.String(64), default='')
    transactionid = db.Column(db.String(64), default='')
    wallet_id = db.Column(db.Integer, db.ForeignKey('wallets.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    createdat = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedat = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    @staticmethod
    def to_dict_col(transactions):
        output = []
        for transaction in transactions:
            data = {
                'id': transaction.id,
                'amount': transaction.amount,
                'action': transaction.action,
                'createdat': transaction.createdat,
                'updatedat':transaction.updatedat,
                'wallet_id': transaction.wallet_id,
                'status': transaction.status,
                'title': transaction.title,
                'description': transaction.status,
                'user_id':transaction.user_id,
                'transactionid':transaction.transactionid
            }
            
            output.append(data)
        return output
    def to_dict(self):
        data = {
                'id': self.id,
                'amount': self.amount,
                'action': self.action,
                'createdat': self.createdat,
                'updatedat':self.updatedat,
                'wallet_id': self.wallet_id,
                'status': self.status,
                'title': self.title,
                'description': self.status,
                'user_id':self.user_id,
                'transactionid':self.transactionid
            }
       
        return data

    def from_dict(self, data):
        for field in ['amount', 'action','title', 'description', 'transactionid','status','wallet_id','user_id']:
            if field in data:
                setattr(self, field, data[field])
