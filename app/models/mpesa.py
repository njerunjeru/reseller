import base64
from datetime import datetime, timedelta
from hashlib import md5
import json
import os
from time import time
from flask import current_app, url_for
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
from app import db, login
from app.search import add_to_index, remove_from_index, query_index
import random
import string

class Mpesa(db.Model):
    
    __tablename__ = 'mpesa_transactions'
    id = db.Column(db.Integer, primary_key=True)
    merchantrequestid = db.Column(db.String(64), index=True)
    checkoutrequestid = db.Column(db.String(64), index=True)
    status = db.Column(db.String(64))
    requestcode = db.Column(db.Integer)
    phonenumber = db.Column(db.String(64))
    transactioncode = db.Column(db.String(64))
    wallet_id = db.Column(db.Integer, db.ForeignKey('wallets.id'))

    @staticmethod
    def generateTransactioncode(stringLength=10):
        """Generate a random string of fixed length """
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for i in range(stringLength))
    
    @staticmethod
    def getPhone(merchantrequestid = '',checkoutrequestid = ''):
        mpesa = Mpesa.query.filter(Mpesa.merchantrequestid == merchantrequestid,Mpesa.checkoutrequestid==checkoutrequestid).first()
        return mpesa.phonenumber
