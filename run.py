
#!/usr/bin/env python
import os
import subprocess
from werkzeug.security import generate_password_hash, check_password_hash
from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager, Shell
from app import create_app, db, cli
from app.models import *
from config import Config

app = create_app()
cli.register(app)

manager = Manager(app)
migrate = Migrate(app, db)


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User}

@manager.command
def recreate_db():
    """
    Recreates a local database. You probably should not use this on
    production.
    """
    db.drop_all()
    db.create_all()
    db.session.commit()

@manager.command
def setup_dev():
    """Runs the set-up needed for local development."""
    setup_general()


@manager.command
def setup_prod():
    """Runs the set-up needed for production."""
    setup_general()
@manager.command
def runserver2():
    app.run(host='192.168.0.240')
def setup_general():
    """Runs the set-up needed for both local development and production.
       Also sets up first admin user."""
       
    Role.insert_roles()
    admin_query = Role.query.filter_by(name='Administrator')
    if admin_query.first() is not None:
        if User.query.filter_by(email=Config.ADMIN_EMAIL).first() is None:
            user = User(
                first_name='Phinehas',
                last_name='Njeru ',
                username='sergewhites',
                password_hash=generate_password_hash(Config.ADMIN_PASSWORD),
                email=Config.ADMIN_EMAIL)
            db.session.add(user)
            db.session.commit()
            print('Added administrator {}'.format(user.full_name()))

if __name__ == '__main__':
    # app.run(host='192.168.0.240')
    manager.run()
